-- BBOX-CALLBACKS --

local P = {} -- package
local args = {...}

local love = love
local os = os
local require = require

return function(bbox)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  bbox[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end


  function love.run()
    if love.math then
      love.math.setRandomSeed(os.time())
    end

    local launchData, err = bbox.io.importLaunchData()
    bbox.io.clearLaunchData()

    if launchData then
      require("editor_main")
    else
      require("launcher_main")
    end

    if fileToRequire then require(fileToRequire) end
   
    if love.load then love.load(arg) end
    if bbox.load then bbox.load(launchData) end
   
    -- We don't want the first frame's dt to include time taken by love.load.
    if love.timer then love.timer.step() end
   
    local dt = 0
   
    -- Main loop time.
    while true do
      -- Process events.
      if love.event then
        love.event.pump()
        for name, a,b,c,d,e,f in love.event.poll() do
          if name == "quit" then
            if not love.quit or not love.quit() then
              return a
            end
          elseif name == "resize" then
            bbox.graphics.invalidate()
          end
          love.handlers[name](a,b,c,d,e,f)
        end
      end
   
      -- Update dt, as we'll be passing it to update
      if love.timer then
        love.timer.step()
        dt = love.timer.getDelta()
      end
   
      -- Call update and draw
      if love.update then love.update(dt) end -- will pass 0 if love.timer is disabled
   
      if bbox.graphics.__INVALIDATED and love.graphics and love.graphics.isActive() then
        bbox.graphics.__INVALIDATED = false
        love.graphics.clear(love.graphics.getBackgroundColor())
        love.graphics.origin()
        if bbox.invalidated then bbox.invalidated() end
        love.graphics.present()
      end
   
      if love.timer then love.timer.sleep(0.001) end
    end
  end

end