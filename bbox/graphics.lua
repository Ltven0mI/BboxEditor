-- BBOX-GRAPHICS --

local P = {} -- package
local args = {...}

local love = love

return function(bbox)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  bbox[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end


  __INVALIDATED = true
  function invalidate()
    __INVALIDATED = true
  end

end