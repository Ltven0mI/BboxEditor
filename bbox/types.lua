-- BBOX-TYPES --

local P = {} -- package
local args = {...}

local string = string
local type = type
local error = error

return function(bbox)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  bbox[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end

  --[[
    checktype(v, typestr) : bool | bool, str
    | Returns true if types match otherwise false and an error message.
    | > v [???] - The value to check type of
    | > typestr [str] - The typename to check for
  ]]
  function checktype(v, typestr)
    if v == nil then
      error("Invalid argument #1 to 'checktype(v, typestr)'. Received 'nil'")
    end
    if type(typestr) ~= "string" then
      error("Type mismatch: Expected 'string' received '"..type(typestr).."'")
    end

    if type(v) == typestr then
      return true
    else
      return false, "Type mismatch: Expected '"..typestr.."' received '"..type(v).."'"
    end
  end

end