-- BBOX-LOG --

local P = {} -- package
local args = {...}

local string = string
local print = print
local debug = debug

return function(bbox)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  bbox[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end

  --[[
    warning(str)
    | > str [str] - Warning string to be printed
  ]]
  function warning(str)
    local info = debug.getinfo(2, "nSl")

    local title = ""
    if info.name then
      -- Function Name, Source and Line Function Defined
      title = string.format("%s (%s) ln:%d", info.name, info.source, info.linedefined)
    else
      -- File Name appended with @, or string from loadstring and Current Line in File
      title = string.format("%s ln:%d", info.source, info.currentline)
    end
    print(string.format("[WRN] %s -> %s", title, str))
  end

end