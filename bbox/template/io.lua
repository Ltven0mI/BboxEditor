-- BBOX-TEMPLATE-IO --

local P = {} -- package
local args = {...}

local BboxLib = BboxLib
local string = string
local io = io

return function(bbox, template)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  template[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end

  --[[
    importTemplate(templatePath) : string | nil, string
    | Returns a string containing the template on success otherwise nil and an error message.
    | > templatePath [str] - The filepath of the template to import
  ]]
  function importTemplate(templatePath)
    -- Type Check templatePath
    local success, err = bbox.types.checktype(templatePath, "string")
    if not success then return nil, err end

    -- Validate File
    if not BboxLib.isFile(templatePath) then
      return nil, "No file found at: '"..templatePath.."'"
    end

    -- Open File
    local file, err = io.open(templatePath, "r")
    if not file then
      return nil, err
    end

    -- Read File
    local templateData = file:read("*all")

    -- Close File
    file:close()

    return templateData
  end

end