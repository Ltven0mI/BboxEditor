-- BBOX-TEMPLATE --

local P = {} -- package
local args = {...}

local require = require

return function(bbox)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  bbox[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end

  _directory = "templates"
  templates = {}

  require(args[1]..".io")(bbox, P) -- Template-IO

end