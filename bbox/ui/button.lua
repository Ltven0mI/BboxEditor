-- BBOX-UI-BUTTON --

local P = {} -- package
local args = {...}

local love = love
local table = table
local setmetatable = setmetatable

return function(bbox, ui)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  ui[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end


  local __base = ui.control

  --[[
    newPrototype()
    |  Returns a prototype made from the __base Control using the variables specific to this Control.
  ]]
  local function newPrototype()
    local t = __base.new(0, 0, 0, 0)
    t.text = ""
    return t
  end
  local pt = newPrototype()
  __prototype = pt

  local mt = {}
  function mt.__index(t, k)
    return pt[k]
  end


  -- Utility Functions --
  function pt:useFont()
    if self.__font then
      love.graphics.setFont(self.__font or ui.__font)
    end
  end

  function pt:unuseFont()
    if self.__font then
      love.graphics.setFont(ui.__font)
    end
  end


  -- Callback Functions --
  function pt:onInvalidated()
    local w, h = self:getDockedSize()

    love.graphics.setColor(255, 255, 255)
    love.graphics.rectangle("fill", 0, 0, w, h)

    self:useFont()

    love.graphics.setColor(0, 0, 0)
    love.graphics.print(self.text, 0, 0)

    self:unuseFont()
  end


  function new(x, y, w, h, text, fontSize)
    local t = newPrototype()
    t.x, t.y = x, y
    t.w, t.h = w, h
    t.text = text
    if fontSize then t.__font = love.graphics.newFont(fontSize) end
    t.__prototype = pt
    t.__base = __base.__prototype
    return setmetatable(t, mt)
  end

end