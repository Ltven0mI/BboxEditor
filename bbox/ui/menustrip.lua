-- BBOX-UI-MENUSTRIP --

local P = {} -- package
local args = {...}

local love = love
local ipairs = ipairs
local type = type
local setmetatable = setmetatable

return function(bbox, ui)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  ui[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end


  local __base = ui.control

  --[[
    NewPrototype()
    |  Returns a prototype made from the __base Control
    |  using the variables specific to this Control.
  ]]
  local function newPrototype()
    local t = __base.new(0, 0, 0, 0)
    t.__menuitems = {}
    return t
  end
  local pt = newPrototype()
  __prototype = pt

  local mt = {}
  function mt.__index(t, k)
    return pt[k]
  end


  -- Utility Functions --

  -- Callback Functions --
  function pt:invalidated()
    self:processChanges()
    local precanvas = love.graphics.getCanvas()
    love.graphics.setCanvas(self.__canvas)
    love.graphics.clear()
    self:handle("onInvalidated")
    self:handle("onDrawItems")
    love.graphics.setCanvas(precanvas)
  end

  function pt:onDrawItems()
    local x = 0
    love.graphics.setColor(255, 255, 255, 255)
    for k, item in ipairs(self.__menuitems) do
      love.graphics.print(item.text, x, 0)
      x = x + ui.__font:getWidth(item.text)
    end
  end


  function new(x, y, w, h)
    local t = newPrototype()
    t.x, t.y = x, y
    t.w, t.h = w, h
    t.__prototype = pt
    t.__base = __base.__prototype
    return setmetatable(t, mt)
  end

end