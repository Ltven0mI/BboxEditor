-- BBOX-UI-CONTAINER --

local P = {} -- package
local args = {...}

local love = love
local ipairs = ipairs
local table = table
local setmetatable = setmetatable

local print = print

return function(bbox, ui)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  ui[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end


  local __base = ui.control

  --[[
    newPrototype()
    |  Returns a prototype made from the __base Control using the variables specific to this Control.
  ]]
  local function newPrototype()
    local t = __base.new(0, 0, 0, 0)
    t.__children = {}
    return t
  end
  local pt = newPrototype()
  __prototype = pt

  local mt = {}
  function mt.__index(t, k)
    return pt[k]
  end


  -- Utility Functions --
  function pt:addControl(control)
    if control ~= self then
      if control.__parent then
        control.__parent:removeControl(control)
      end
      table.insert(self.__children, control)
      control.__parent = self
      self:invalidate()
      return true
    end
    return nil, "Control cannot be added as a child to itself!"
  end

  function pt:removeControl(control)
    for k, v in ipairs(self.__children) do
      if v == control then
        table.remove(self.__children, k)
        control.__parent = nil
        return true
      end
    end
    return nil, "Child Control not found!"
  end


  -- Callback Functions --
  function pt:invalidated()
    self:processChanges()
    local precanvas = love.graphics.getCanvas()
    love.graphics.setCanvas(self.__canvas)
    love.graphics.clear()
    self:handle("onInvalidated")
    for k, child in ipairs(self.__children) do
      if child.__invalidated then
        child.__invalidated = false
        child:handle("invalidated")
      end
      love.graphics.setColor(255, 255, 255, 255)
      love.graphics.draw(child.__canvas, child.dockedX, child.dockedY)
    end
    love.graphics.setCanvas(precanvas)
  end

  function pt:parentResized()
    if self.__dock then
      self.__sizechanged = true
      self:invalidate()
      for k, child in ipairs(self.__children) do
        child:handle("parentResized")
      end
    end
  end

  function pt:mousepressed(x, y, ...)
    local dockX, dockY = self:getDockedPosition()
    if self:contains(x, y) then
      local relX, relY = x - dockX, y - dockY
      self:handle("onMousePressed", relX, relY, ...)
      for k, child in ipairs(self.__children) do
        child:handle("mousepressed", relX, relY, ...)
      end
    end
  end


  function new(x, y, w, h)
    local t = newPrototype()
    t.x, t.y = x, y
    t.w, t.h = w, h
    t.__prototype = pt
    t.__base = __base.__prototype
    return setmetatable(t, mt)
  end

end