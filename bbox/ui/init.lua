-- BBOX-UI --

local P = {} -- package
local args = {...}

local love = love
local require = require

return function(bbox)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  bbox[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end

  -- Dock Types --
  DOCK_NONE   = 0
  DOCK_LEFT   = 1
  DOCK_RIGHT  = 2
  DOCK_TOP    = 3
  DOCK_BOTTOM = 4
  DOCK_ALL    = 5

  __font = love.graphics.getFont()

  require(args[1]..".control")(bbox, P)   -- UI-Control
  require(args[1]..".container")(bbox, P) -- UI-Container
  require(args[1]..".button")(bbox, P)    -- UI-Button
  require(args[1]..".menustrip")(bbox, P) -- UI-MenuStrip

end