-- BBOX-UI-CONTROL --

local P = {} -- package
local args = {...}

local love = love
local type = type
local ipairs = ipairs
local setmetatable = setmetatable

return function(bbox, ui)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  ui[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end


  local __base = nil

  --[[
    newPrototype()
    |  Returns a prototype made using the variables specific to this Control.
  ]]
  local function newPrototype()
    return {
      x=0, y=0,
      w=0, h=0,
      dockedX=0, dockedY=0,
      dockedW=0, dockedH=0,
      margin={
        top=0,
        bottom=0,
        left=0,
        right=0
      },
      __invalidated=true,
      __poschanged=true,
      __sizechanged=true,
      __canvas=nil,
      __parent=nil,
      __dock=nil
    }
  end
  local pt = newPrototype()
  __prototype = pt

  mt = {}
  function mt.__index(t, k)
    return pt[k]
  end


  --[[
    contains(x, y) : bool
    | Returns true if x and y are inside Control otherwise false
    | > x [num] - The x coordinate relative to parent
    | > y [num] - The y coordinate relative to parent
  ]]
  function pt:contains(x, y)
    local dockX, dockY = self:getDockedPosition()
    local dockW, dockH = self:getDockedSize()
    return (x >= dockX and x < dockX + dockW and
      y >= dockY and y < dockY + dockH)
  end


  -- Setters-Getters --
  function pt:setPosition(x, y)
    if x ~= self.x or y ~= self.y then
      self.x = x or self.x
      self.y = y or self.y
      __poschanged = true
      self:invalidate()
    end
  end

  function pt:getPosition()
    return self.x, self.y
  end

  function pt:setSize(w, h)
    if w ~= self.w or h ~= self.h then
      self.w = w or self.w
      self.h = h or self.h
      self.__sizechanged = true
      self:invalidate()
    end
  end

  function pt:getSize()
    return self.w, self.h
  end

  function pt:setMargin(t, b, l, r)
    self.margin.top = t or self.margin.top
    self.margin.bottom = b or self.margin.bottom
    self.margin.left = l or self.margin.left
    self.margin.right = r or self.margin.right
    if self.__dock then
      self.__sizechanged = true
      self:invalidate()
    end
  end

  function pt:setDock(...)
    local newDock = {
      l=false,
      r=false,
      t=false,
      b=false
    }
    local dockTypeNotNone = false
    for k, dockType in ipairs({...}) do
      if dockType == ui.DOCK_LEFT then
        newDock.l = true
        dockTypeNotNone = true
      elseif dockType == ui.DOCK_RIGHT then
        newDock.r = true
        dockTypeNotNone = true
      elseif dockType == ui.DOCK_TOP then
        newDock.t = true
        dockTypeNotNone = true
      elseif dockType == ui.DOCK_BOTTOM then
        newDock.b = true
        dockTypeNotNone = true
      elseif dockType == ui.DOCK_ALL then
        newDock.l = true
        newDock.r = true
        newDock.t = true
        newDock.b = true
        dockTypeNotNone = true
      end
    end

    if dockTypeNotNone then
      self.__dock = newDock
    else
      self.__dock = nil
    end

    self.__sizechanged = true
    self:invalidate()
  end

  function pt:getDockedPosition()
    return self.dockedX, self.dockedY
  end

  function pt:getDockedSize()
    return self.dockedW, self.dockedH
  end

  function pt:getParentSize()
    if self.__parent then
      return self.__parent:getDockedSize()
    else
      return love.graphics.getDimensions()
    end
  end


  -- Utility Functions --
  function pt:handle(callback, ...)
    if self[callback] and type(self[callback]) == "function" then
      self[callback](self, ...)
    end
  end

  function pt:invalidate()
    self.__invalidated = true
    if self.__parent and not self.__parent.__invalidated then
      self.__parent:invalidate()
    elseif not self.__parent then
      bbox.graphics.invalidate()
    end
  end

  function pt:resizeCanvas()
    local w, h = self:getDockedSize()
    if w > 0 and h > 0 then
      self.__canvas = love.graphics.newCanvas(w, h)
    end
  end

  function pt:updateDockedPosition()
    local x, y = self.x, self.y
    if self.__dock then
      local parentW, parentH = self:getParentSize()
      local dock = self.__dock
      if dock.t then
        y = self.margin.top
      elseif dock.b then
        y = parentH - self.h - self.margin.bottom
      end
      if dock.l then
        x = self.margin.left
      elseif dock.r then
        x = parentW - self.w - self.margin.right
      end
    end
    self.dockedX = x
    self.dockedY = y
  end

  function pt:updateDockedSize()
    local w, h = self.w, self.h
    if self.__dock then
      local parentW, parentH = self:getParentSize()
      local dock = self.__dock
      if dock.t and dock.b then h = parentH - self.margin.top - self.margin.bottom end
      if dock.l and dock.r then w = parentW - self.margin.left - self.margin.right end
    end
    self.dockedW = w
    self.dockedH = h
  end

  function pt:processChanges()
    if self.__poschanged then
      self.__poschanged = false
      if not self.__sizechanged then
        self:updateDockedPosition()
      end
    end

    if self.__sizechanged then
      self.__sizechanged = false
      self:updateDockedPosition()
      self:updateDockedSize()
      self:resizeCanvas()
    end
  end


  -- Callback Functions --
  function pt:invalidated()
    self:processChanges()
    local precanvas = love.graphics.getCanvas()
    love.graphics.setCanvas(self.__canvas)
    love.graphics.clear()
    self:handle("onInvalidated")
    love.graphics.setCanvas(precanvas)
  end

  function pt:parentResized(w, h)
    if self.__dock then
      self.__sizechanged = true
      self:invalidate()
    end
  end

  function pt:mousepressed(x, y, ...)
    local dockX, dockY = self:getDockedPosition()
    if self:contains(x, y) then
      self:handle("onMousePressed", relX, relY, ...)
    end
  end


  function new(x, y, w, h)
    local t = newPrototype()
    t.x, t.y = x, y
    t.w, t.h = w, h
    t.__prototype = pt
    return setmetatable(t, mt)
  end

end