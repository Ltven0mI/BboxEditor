-- BBOX-IO --

local P = {} -- package
local args = {...}

local love = love
local os = os
local io = io
local type = type
local loadfile = loadfile
local pairs = pairs
local string = string
local table = table
local tostring = tostring


return function(bbox)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  bbox[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end


  --[[
    importLua(filepath)
    | Returns file's return on success otherwise nil and an error message.
    | > filepath [str] - The filepath of a valid lua file
  ]]
  function importLua(filepath, ...)
    -- Type Check filepath
    local success, err = bbox.types.checktype(filepath, "string")
    if not success then return nil, err end

    -- Load File
    local func, msg = loadfile(filepath)
    if func == nil then
      return nil, "Failed to load lua file '"..filepath.."': "..msg
    end

    -- Return result
    return func(...)
  end
  

  --[[
    importFile(filepath)
    | Returns file's content on success otherwise nil and an error message.
    | > filepath [str] - The filepath of the file to import
  ]]
  function importFile(filepath)
    -- Type Check filepath
    local success, err = bbox.types.checktype(filepath, "string")
    if not success then return nil, err end

    -- Open File
    local file, err = io.open(filepath, "r")
    if file == nil then
      return nil, "Failed to open file: "..err
    end

    -- Read File Content
    local content = file:read("*all")
    -- Close File
    file:close()

    -- Return File Content
    return content
  end


  --[[
    exportLua(filepath, dataTable) : boolean
    | Returns true on success otherwise nil and an error message
    | > filepath [str] - The filepath to write the serialized data to
    | > dataTable [tbl] - A table to be serialized to a string as a lua 
  ]]
  function exportLua(filepath, dataTable)
    -- Type Check filepath
    local success, err = bbox.types.checktype(filepath, "string")
    if not success then return nil, err end

    -- Serialize Data
    local serializedData, msg = serializeLua(dataTable)
    if serializedData == nil then
      return nil, "Failed to serialize data: "..msg
    end

    -- Open File
    local file = io.open(filepath, "w")
    if file == nil then
      return nil, "Failed to open file '"..filepath.."'"
    end

    -- Write To File
    file:write(serializedData)

    -- Close File
    file:close()

    return true
  end


   --[[
    exportFile(filepath, data)
    | Writes 'data' to 'filepath' and returns true on success otherwise nil and an error message.
    | > filepath [str] - The filepath to write 'data' to
    | > data [str] - The data to write
  ]]
  function exportFile(filepath, data)
    -- Type Check filepath
    local success, err = bbox.types.checktype(filepath, "string")
    if not success then return nil, err end

    -- Type Check data
    local success, err = bbox.types.checktype(data, "string")
    if not success then return nil, err end

    -- Open File
    local file, err = io.open(filepath, "w")
    if file == nil then
      return nil, "Failed to open file: "..err
    end

    -- Write File Content
    file:write(data)
    -- Close File
    file:close()

    -- Return true for success
    return true
  end


  --[[
    serializeKey(k) : string
    | Returns a string containing the key for use in a table string.
    | > k [?] - The key to be serialized
    | [NOTE] Only Number and String are supported. Other types converted to string
  ]]
  function serializeKey(k)
    local k_type = type(k)
    if k_type == "number" then
      return "["..k.."]"
    elseif k_type == "string" then
      return k
    end
    return "[\""..tostring(k).."\"]"
  end


  --[[
    serializeValue(v) : string
    | Returns a string containing the value for use in a table string.
    | > v [?] - The value to be serialized
    | [NOTE] Only Number and String are supported otherwise nil is returned
  ]]
  function serializeValue(v)
    local v_type = type(v)
    if v_type == "number" then
      return v
    elseif v_type == "string" then
      return "\""..v.."\""
    end
    return nil
  end


  --[[
    tableToString(t[, indentation]) : string
    | Returns a string containing the table string.
    | > t [tbl] - The table to be serialized
    | > indent [str] - The indentation to apply to the entries. "  " if left out
  ]]
  function tableToString(t, indent)
    local indent = indent or ""
    local indentation = indent.."  "
    local entries = {"{\n"}

    -- Parse Table
    for k, v in pairs(t) do
      -- Check Value Type
      local v_type = type(v)
      if v_type == "string" or v_type == "number" or v_type == "table" then

        -- Serialize Key
        local key_str = serializeKey(k)

        -- Serialize Value
        local value_str = nil
        if type(v) ~= "table" then
          value_str = serializeValue(v)
        else
          value_str = tableToString(v, indentation)
        end

        -- Add Entry
        local entry = string.format("%s%s=%s,\n", indentation, key_str, value_str)
        table.insert(entries, entry)
      end
    end
    table.insert(entries, indent.."}")

    -- Return Result String
    return table.concat(entries)
  end


  --[[
    serializeLua(dataTable) : string
    | Returns a string containing the serialized data in a lua table format.
    | > dataTable [tbl] - A table to be serialized to a string representation of the table
    | [NOTE] Only Number, String and Boolean are supported.
    | [NOTE] Key is assumed a string.
  ]]
  function serializeLua(dataTable)
    -- Type Check dataTable
    local success, err = bbox.types.checktype(dataTable, "table")
    if not success then return nil, err end

    -- Return Serialized Data
    return "return "..tableToString(dataTable)
  end


  -- Launch Data --

  --[[
    importLaunchData() : string
    | Returns data imported from LaunchData on success otherwise nil and an error message.
  ]]
  function importLaunchData()
    -- Load LaunchData File
    local data, err = love.filesystem.read("launch.data")
    if data == nil then
      return nil, "Failed to load LaunchData File: "..err
    end

    -- Return data
    return data
  end

  --[[
    exportLaunchData(data) : boolean
    | Returns true on success otherwise false and an error message.
    | > data [str] - The data to write to LaunchData File
  ]]
  function exportLaunchData(data)
    -- Type Check data
    local success, err = bbox.types.checktype(data, "string")
    if not success then return nil, err end

    -- Write Launch Data to File
    local success, err = love.filesystem.write("launch.data", data)
    if not success then
      return false, "Failed to write to LaunchData File: "..err
    end

    -- Return true for success
    return true
  end

  --[[
    clearLaunchData() : boolean
    | Returns true if successfully cleared LaunchData otherwise false
  ]]
  function clearLaunchData()
    return love.filesystem.remove("launch.data")
  end

end