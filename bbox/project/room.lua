-- BBOX-PROJECT-ROOM --

local P = {} -- package
local args = {...}

local BboxLib = BboxLib
local type = type
local string = string
local loadstring = loadstring

return function(bbox, project)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  project[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end


  local function literalize(str)
    return str:gsub("[%(%)%.%%%+%-%*%?%[%]%^%$]", function(c) return "%" .. c end)
  end

  --[[
    buildRoom(room, proj) : string | nil, string
    | Returns the built room on success otherwise nil and an error message.
    | > room [tbl] - The room to be built
    | > proj [tbl] - The project 'room' is from
  ]]
  function buildRoom(room, proj)
    -- Type Check room
    local success, err = bbox.types.checktype(room, "table")
    if not success then return nil, err end
    -- Type Check proj
    local success, err = bbox.types.checktype(proj, "table")
    if not success then return nil, err end

    -- Load Room Script
    local loadedScript, err = loadRoomScript(room.scriptPath, proj.__path)
    if loadedScript == nil then
      return nil, "Failed to load script for room '"..room.name.."' "..err
    end

    -- Get Room Template
    local loadedTemplate = proj.__loadedTemplates[room.templateName]
    if loadedTemplate == nil then
      return nil, "Project '"..proj.name.."' has no loaded template '"..room.templateName.."'"
    end

    -- Escape magic characters in loadedScript
    loadedScript = literalize(loadedScript)

    -- Combine Template and Script
    local roomCode = string.gsub(loadedTemplate, "##CODE##", loadedScript)

    -- Syntax Check RoomCode
    local func, err = loadstring(roomCode)
    if func == nil then
      return nil, "Syntax error: "..err
    end

    -- Return RoomCode
    return roomCode
  end


  --[[
    loadRoomScript(scriptPath, projectPath) : string | nil, string
    | Returns the loaded script on success otherwise nil and an error message.
    | > scriptPath [str] - The path to the script.lua file relative to 'projectPath'
    | > projectPath [str] - The path to the project the script is being loaded from
  ]]
  function loadRoomScript(scriptPath, projectPath)
    -- Type Check scriptPath
    local success, err = bbox.types.checktype(scriptPath, "string")
    if not success then return nil, err end

    -- Type Check projectPath
    local success, err = bbox.types.checktype(projectPath, "string")
    if not success then return nil, err end

    -- Import Script
    local fullPath = projectPath.."/"..scriptPath
    local scriptContent, err = bbox.io.importFile(fullPath)
    if scriptContent == nil then
      return nil, "Failed to import script at '"..fullPath.."' : "..err
    end

    -- Return Script Content
    return scriptContent
  end

end