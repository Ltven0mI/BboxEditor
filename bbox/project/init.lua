-- BBOX-PROJECT --

local P = {} -- package
local args = {...}

local require = require
local setmetatable = setmetatable

return function(bbox)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  bbox[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end

  --[[ Project
    name:s:projectname
    templates:[s]:templates[name]
    rooms:[s,s]:rooms[name, path]
    startroom:s:pathtoroomfile
  ]]

  --[[ Room
    template:s:templateName
    script:s:pathtoscriptfile
  ]]

  require(args[1]..".io")(bbox, P)    -- Project-IO
  require(args[1]..".room")(bbox, P)  -- Project-Room


  --[[
    newPrototype()
    |  Returns a prototype made using default variables.
  ]]
  local function newPrototype()
    return {}
  end
  local pt = newPrototype()
  __prototype = pt

  mt = {}
  function mt.__index(t, k)
    return pt[k]
  end

  --[[
    close() : bool | nil, string
    | Returns true on success otherwise nil and an error message.
    | #NOTE# Closes all file handles.
  ]]
  function pt:close()
    local success, err = io.closeProject(self)
    if not success then
      return nil, "Failed to close project: "..err
    end
    return true
  end


  --[[
    open(projectPath) : table | nil, string
    | Returns the opened project on success otherwise nil and an error message.
    | > projectPath [str] - The path to a folder containing an 'info.bbe' file
  ]]
  function open(projectPath)
    local t = newPrototype()

    local success, err = io.openProject(t, projectPath)
    if not success then
      return nil, "Failed to open project: "..err
    end

    t.__prototype = pt
    return setmetatable(t, mt)
  end

  _loadedProjects = {}

end