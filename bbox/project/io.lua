-- BBOX-PROJECT-IO --

local P = {} -- package
local args = {...}

local BboxLib = BboxLib
local path = path
local json = json
local pairs = pairs
local type = type
local io = io
local loadstring = loadstring
local pcall = pcall

return function(bbox, project)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  project[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end


  -- Info File Properties --
  local PROPERTIES_INFO = {
    name="string",
    rooms="table",
    templates="table",
    startRoom="string"
  }

  -- Room File Properties --
  local PROPERTIES_ROOM = {
    script="string",
    template="string"
  }


  local function checkProperties(t, properties)
    for pKey, pType in pairs(properties) do
      local v = t[pKey]
      if v == nil or type(v) ~= pType then
        return nil, "Invalid or missing property '"..pKey.."'"
      end
    end
    return true
  end

  local function openTableFile(path, properties)
    -- Create File Handle
    local fileHandle, err = io.open(path, "r")
    if fileHandle == nil then
      return nil, "Failed to open file: "..err
    end

    -- Load infoTable
    local func, err = loadstring(fileHandle:read("*all"))
    if func == nil then
      return nil, "Failed to load '"..path.."' "..err
    end

    -- Call func
    local success, table_err = pcall(func)
    if not success then
      return nil, "Failed to call '"..path.."' "..err
    end

    -- Type Check fileTable
    local fileTable = table_err
    if type(fileTable) ~= "table" then
      return nil, "File '"..path.."' returned '"..type(fileTable).."' expected 'table'"
    end

    -- Properties Check fileTable
    local isValid, err = checkProperties(fileTable, properties)
    if not isValid then
      return nil, "File missing property: "..err
    end

    return fileHandle, fileTable
  end


  --[[
    openProject(proj, projectPath) : table | nil, string
    | Returns the opened project on success otherwise nil and an error message.
    | > proj [tbl] - Table to put project data into
    | > projectPath [str] - The path to a folder containing an 'info.bbe'file
  ]]
  function openProject(proj, projectPath)
    -- Type Check proj
    local success, err = bbox.types.checktype(proj, "table")
    if not success then return nil, err end
    -- Type Check projectPath
    local success, err = bbox.types.checktype(projectPath, "string")
    if not success then return nil, err end

    -- Check ProjectPath is directory
    if not BboxLib.isDirectory(projectPath) then
      return nil, "No directory found at '"..projectPath.."'"
    end

    -- Create Project Table
    proj.__path = projectPath

    -- Open Info File
    local infoPath = projectPath.."/info.bbe"
    local success, err = openProjectInfoFile(infoPath, proj)
    if not success then
      return nil, err
    end

    -- Open Room Files
    local success, err = openProjectRoomFiles(proj.__roomPaths, proj)
    if not success then
      return nil, err
    end

    return proj
  end

  --[[
    closeProject(proj) : bool | nil, string
    | Returns true on success or nil and an error message.
    | > proj [tbl] - Project to close
  ]]
  function closeProject(proj)
    -- Type Check proj
    local success, err = bbox.types.checktype(proj, "table")
    if not success then return nil, err end

    -- Close Info File
    local success, err = closeProjectInfoFile(proj)
    if not success then
      return nil, "Failed to close info file: "..err
    end

    -- Close Room Files
    local success, err = closeProjectRoomFiles(proj)
    if not success then
      return nil, "Failed to close room files: "..err
    end

    -- Return true for success
    return true
  end


  --[[
    openProjectInfoFile(path, proj) : bool | nil, string
    | Returns true on success otherwise nil and an error message.
    | > path [str] - The path to the 'info.bbe' file to open
    | > proj [tbl] - The open project to use
  ]]
  function openProjectInfoFile(path, proj)
    -- Type Check path
    local success, err = bbox.types.checktype(path, "string")
    if not success then return nil, err end
    -- Type Check proj
    local success, err = bbox.types.checktype(proj, "table")
    if not success then return nil, err end

    local infoHandle, infoTable = openTableFile(path, PROPERTIES_INFO)
    if not infoHandle then
      return nil, infoTable
    end
    proj.__infoHandle = infoHandle
    proj.__infoTable = infoTable

    proj.name = infoTable.name

    proj.__roomPaths = infoTable.rooms
    proj.__templatePaths = infoTable.templates
    proj.__startRoom = infoTable.startRoom

    return true
  end

  --[[
    closeProjectInfoFile(proj) : bool | nil, string
    | Returns true on success otherwise nil and an error message.
    | proj [tbl] - Opened project to close info file of
  ]]
  function closeProjectInfoFile(proj)
    -- Type Check proj
    local success, err = bbox.types.checktype(proj, "table")
    if not success then return nil, err end

    -- Close info file
    if proj.__infoHandle then
      proj.__infoHandle:close()
    end

    -- Clean up proj
    proj.__infoHandle = nil
    proj.__infoTable = nil

    -- Return true for success
    return true
  end


  --[[
    openProjectRoomFiles(roomPaths, proj) : bool | nil, string
    | Returns true on success otherwise nil and an error message.
    | > roomPaths [tbl] - A table where k = roomName and v = roomPath
    | > proj [tbl] - The open project to use
  ]]
  function openProjectRoomFiles(roomPaths, proj)
    -- Type Check roomPaths
    local success, err = bbox.types.checktype(roomPaths, "table")
    if not success then return nil, err end
    -- Type Check proj
    local success, err = bbox.types.checktype(proj, "table")
    if not success then return nil, err end

    local roomInfoTables = {}
    for name, path in pairs(roomPaths) do
      -- Open Table File
      local fullPath = proj.__path.."/"..path
      local roomHandle, roomTable = openTableFile(fullPath, PROPERTIES_ROOM)
      if not roomHandle then
        return nil, "Failed to open room '"..name.."' "..roomTable
      end
      -- Create Room Table
      local roomInfoTable = {
        name=name,
        __path=path,
        scriptPath=roomTable.script,
        templateName=roomTable.template,
        __infoHandle=roomHandle,
        __infoTable=roomTable
      }
      -- Set Room
      roomInfoTables[name] = roomInfoTable
    end

    proj.roomInfoTables = roomInfoTables

    -- Return true for success
    return true
  end

  --[[
    closeProjectRoomFiles(proj) : bool | nil, string
    | Returns true on success otherwise nil and an error message.
    | > proj [tbl] - Opened project to close room files of
  ]]
  function closeProjectRoomFiles(proj)
    -- Type Check proj
    local success, err = bbox.types.checktype(proj, "table")
    if not success then return nil, err end

    for name, roomInfoTable in pairs(proj.roomInfoTables) do
      -- Close room file
      if roomInfoTable.__infoHandle then
        roomInfoTable.__infoHandle:close()
      end
    end

    proj.roomInfoTables = nil

    -- Return true for success
    return true
  end


  --[[
    loadProjectInfoFile(path) : table | nil, string
    | Returns loaded projectInfoTable on success otherwise nil and an error message.
    | > path [str] - The path to the 'info.bbe' file to open
    | #NOTE# Does not keep file open.
  ]]
  function loadProjectInfoFile(path)
    -- Type Check path
    local success, err = bbox.types.checktype(path, "string")
    if not success then return nil, err end

    -- Open Info File
    local infoHandle, infoTable = openTableFile(path.."/info.bbe", PROPERTIES_INFO)
    if not infoHandle then
      return nil, infoTable
    end

    -- Close Info File
    infoHandle:close()

    -- Return infoTable for success
    return infoTable
  end


  --[[
    loadTemplate(templatePath, projectPath) : string | nil, string
    | Returns the loaded template on success otherwise nil and an error message.
    | > templatePath [str] - The path to the template.lua file relative to 'projectPath'
    | > projectPath [str] - The path to the project the template is being loaded from
  ]]
  function loadTemplate(templatePath, projectPath)
    -- Type Check templatePath
    local success, err = bbox.types.checktype(templatePath, "string")
    if not success then return nil, err end

    -- Type Check projectPath
    local success, err = bbox.types.checktype(projectPath, "string")
    if not success then return nil, err end

    -- Import Template
    local fullPath = projectPath.."/"..templatePath
    local templateContent, err = bbox.io.importFile(fullPath)
    if templateContent == nil then
      return nil, "Failed to import template at '"..fullPath.."' "..err
    end

    -- Return Template Content
    return templateContent
  end


  --[[
    Builder Standard:
    A builder must return a function which will be called on load
    The function must return a table containing the builder
    The builder must contain a function called build that takes the project and a buildpath
  ]]


  --[[
    loadBuilder(builderPath) : table | nil, string
    | Returns the loaded builder on success otherwise nil and an error message.
    | > builderPath [str] - The path to a lua file which conforms to the Builder Standard
  ]]
  function loadBuilder(builderPath)
    -- Type Check builderPath
    local success, err = bbox.types.checktype(builderPath, "string")
    if not success then return nil, err end

    -- Import Builder
    local builderFunc, err = bbox.io.importLua(builderPath)
    if builderFunc == nil then
      return nil, "Failed to load builder: "..err
    end

    local builderDir = path.getDirectory(builderPath)

    -- Initiate Builder
    local builder, err = builderFunc(bbox, builderDir)
    if builder == nil then
      return nil, "Failed to init builder: "..err
    end

    -- Return Builder
    return builder
  end

end