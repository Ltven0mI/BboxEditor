-- BBOX-INIT --

local P = {} -- package
local args = {...}

local print = print
local require = require
local setmetatable = setmetatable

local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
_G[label] = P
_VERSIONNUM = _VERSION:gsub("Lua ", "")
_VERSIONNUM = tonumber(_VERSIONNUM)
if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end


require(args[1]..".types")(P)     -- Types
require(args[1]..".log")(P)       -- Log
require(args[1]..".io")(P)        -- IO
require(args[1]..".callbacks")(P) -- Callbacks
require(args[1]..".graphics")(P)  -- Graphics
require(args[1]..".ui")(P)        -- UI
require(args[1]..".project")(P)   -- Project
require(args[1]..".template")(P)  -- Template