version = {0, 7, 3, 1}
local args = {...}
if args[1] == "true" then
  local lines = {}
  for line in io.lines("version.lua") do table.insert(lines, line) end
  local v = {version[1], version[2], version[3], version[4]}
  v[4] = v[4] + 1
  v[3] = v[3] + math.floor(v[4] / 10)
  v[4] = v[4] % 10
  v[2] = v[2] + math.floor(v[3] / 10)
  v[3] = v[3] % 10
  v[1] = v[1] + math.floor(v[2] / 10)
  v[2] = v[2] % 10
  lines[1] = "version = {"..v[1]..", "..v[2]..", "..v[3]..", "..v[4].."}"
  local f = io.open("version.lua", "w")
  for k, line in ipairs(lines) do f:write(line.."\n") end
  f:close()
end
return table.concat(version, ".")
