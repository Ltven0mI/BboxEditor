love.window.setTitle("BboxEditor - Projects")

local palette = {
  bg={22, 22, 22},
  fg={58, 62, 73},
  fgdark={40, 45, 56},
  fgshadow={0, 0, 0},
  text={220, 220, 220},
  textdark={95, 95, 95}
}


local p = {}


local function defaultButtonOnInvalidated(self)
  local w, h = self:getDockedSize()

  love.graphics.setColor(palette.fgshadow)
  love.graphics.rectangle("fill", 3, 3, w - 3, h - 3)

  love.graphics.setColor(palette.fg)
  love.graphics.rectangle("fill", 0, 0, w - 3, h - 3)

  self:useFont()

  love.graphics.setColor(palette.text)
  love.graphics.print(self.text, 15, 12)

  self:unuseFont()
end


-- Base Container --
local container = bbox.ui.container.new(0, 0, 0, 0)
container:setDock(bbox.ui.DOCK_ALL)
function container:onInvalidated()
  local w, h = self:getDockedSize()

  love.graphics.setColor(palette.bg)
  love.graphics.rectangle("fill", 0, 0, w, h)
end

local headerFont = love.graphics.newFont(32)

-- Header Container -- ('Projects', Open, New)
local headerContainer = bbox.ui.container.new(0, 0, 0, 128)
headerContainer:setDock(bbox.ui.DOCK_LEFT, bbox.ui.DOCK_RIGHT, bbox.ui.DOCK_TOP)
function headerContainer:onInvalidated()
  local w, h = self:getDockedSize()

  love.graphics.setColor(palette.fgshadow)
  love.graphics.rectangle("fill", 19, 35, w - 464, 64)

  love.graphics.setColor(palette.fg)
  love.graphics.rectangle("fill", 16, 32, w - 464, 64)

  love.graphics.setFont(headerFont)
  love.graphics.setColor(palette.text)
  love.graphics.print("Projects", 28, 52)
  love.graphics.setFont(bbox.ui.__font)
end
container:addControl(headerContainer)

-- Open Button --
local openButton = bbox.ui.button.new(0, 0, 163, 51, "Open", 24)
openButton:setDock(bbox.ui.DOCK_RIGHT, bbox.ui.DOCK_TOP)
openButton:setMargin(32, 0, 0, 205)
openButton.onInvalidated = defaultButtonOnInvalidated
function openButton:onMousePressed(x, y, btn)
  local filepath, err = BboxLib.openFileDialog({filter="*.bbe"})
  if filepath ~= nil then
    local projectPath = path.getDirectory(filepath)
    local infoTable, err = bbox.project.io.loadProjectInfoFile(projectPath)
    if infoTable then
      p.addProjectEntry(infoTable.name, projectPath)
    else
      bbox.log.warning("Attempt to open invalid project: "..err)
    end
  end
end
headerContainer:addControl(openButton)

-- New Button --
local newButton = bbox.ui.button.new(0, 0, 163, 51, "New", 24)
newButton:setDock(bbox.ui.DOCK_RIGHT, bbox.ui.DOCK_TOP)
newButton:setMargin(32, 0, 0, 13)
newButton.onInvalidated = defaultButtonOnInvalidated
function newButton:onMousePressed(x, y, btn)
  print("NEW")
end
headerContainer:addControl(newButton)


-- Body Container -- (Project List)
local bodyContainer = bbox.ui.container.new(0, 0, 0, 0)
bodyContainer:setDock(bbox.ui.DOCK_ALL)
bodyContainer:setMargin(160, 0, 0, 0)
function bodyContainer:onInvalidated()
  local w, h = self:getDockedSize()
end
container:addControl(bodyContainer)

local function projectButtonOnMousePressed(self)
  print(self.__projectEntry.name, self.__projectEntry.path)
  local success, err = bbox.io.exportLaunchData(self.__projectEntry.path)
  if not success then
    bbox.log.warning("Failed to export LaunchData: "..err)
  end
  love.event.quit("restart")
end

local function projectButtonOnInvalidated(self)
  local w, h = self:getDockedSize()

  -- love.graphics.rectangle("line", 0, 0, w-1, h-1)

  love.graphics.setColor(palette.fgshadow)
  love.graphics.rectangle("fill", 3, 3, 384, 64)

  love.graphics.setColor(palette.fg)
  love.graphics.rectangle("fill", 0, 0, 384, 64)

  self:useFont()

  love.graphics.setColor(palette.text)
  love.graphics.print(self.text, 10, 5)

  self:unuseFont()


  love.graphics.setColor(palette.fgshadow)
  love.graphics.rectangle("fill", 35, 51, 512, 32)

  love.graphics.setColor(palette.fgdark)
  love.graphics.rectangle("fill", 32, 48, 512, 32)

  love.graphics.setColor(palette.textdark)
  love.graphics.print(self.__projectEntry.path, 41, 56)
end

local function createProjectButton(projectEntry, projectList)
  local projectButton = bbox.ui.button.new(0, 0, 539, 96, projectEntry.name, 32)
  projectButton:setDock(bbox.ui.DOCK_TOP, bbox.ui.DOCK_LEFT)
  projectButton:setMargin((#projectList-1) * 96, 0, 32, 0)
  projectButton.onInvalidated = projectButtonOnInvalidated
  projectButton.onMousePressed = projectButtonOnMousePressed
  projectButton.__projectEntry = projectEntry
  bodyContainer:addControl(projectButton)
end


local projectList = nil

local function serializeString(str)
  str = str:gsub(":", "#:")
  return str
end

local function deserializeString(str)
  str = str:gsub("#:", ":")
  return str
end

function p.addProjectEntry(name, path)
  local projectEntry = {name=name, path=path}
  table.insert(projectList, projectEntry)
  createProjectButton(projectEntry, projectList)
end


-- Load Project List --
local function loadProjectList()
  projectList = {}
  if love.filesystem.exists("projects.lst") then
    for line in love.filesystem.lines("projects.lst") do
      if line ~= "" then
        local path = deserializeString(line)
        local infoTable, err = bbox.project.io.loadProjectInfoFile(path)
        if infoTable == nil then
          bbox.log.warning("Discarding project list entry: path='"..path.."' reason: "..err)
        else
          p.addProjectEntry(infoTable.name, path)
        end
      else
        bbox.log.warning("ProjectList failed to parse line: \""..line.."\"")
      end
    end
  end
end

local function saveProjectList()
  local entryStrings = {}
  for k, entry in ipairs(projectList) do
    local path = serializeString(entry.path)
    table.insert(entryStrings, path)
  end
  local fileData = table.concat(entryStrings, "\n")
  local success, err = love.filesystem.write("projects.lst", fileData)
  if not success then
    bbox.log.warning("Failed to write to 'projects.lst' "..err)
  end
end


function love.load(arg)
  loadProjectList()
  -- addProjectEntry("test", "memes:")
  -- saveProjectList()
end

function love.quit()
  saveProjectList()
end

function bbox.invalidated()
  if container.__invalidated then
    container.__invalidated = false
    container:handle("invalidated")
  end
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.draw(container.__canvas, container.dockedX, container.dockedY)
end

function love.mousepressed(...)
  container:handle("mousepressed", ...)
end

function love.resize(w, h)
  container:handle("parentResized")
  bbox.graphics.invalidate()
end