--[[
  MIT License

  Copyright (c) 2017 Wade Rauschenbach

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
]]

-- This version of FileZip is made for Lua +5.1

local P = {}   -- package
local args = {...}

local CRC32 = require(args[1].."/CRC32")

local type = type
local error = error
local ipairs = ipairs
local print = print
local pcall = pcall
local require = require
local tostring = tostring
local io = io
local table = table
local string = string

local label = args[1]:sub((args[1]:find("%.[^%.]*$") + 1) or 0)
_G[label] = P
local LUAVERSION = _VERSION:gsub("Lua ", "")
LUAVERSION = tonumber(LUAVERSION)
if LUAVERSION >= 5.3 then _ENV = P else setfenv(1, P) end


--[[
Requires the first module listed that exists, else raises like `require`.
If a non-string is encountered, it is returned.
Second return value is module name loaded (or '').
--]]
local function requireany(...)
  local errs = {}
  for _,name in ipairs{...} do
    if type(name) ~= 'string' then return name, '' end
    local ok, mod = pcall(require, name)
    if ok then return mod, name end
    errs[#errs+1] = mod
  end
  error(table.concat(errs, '\n'), 2)
end

local bit, name_ = requireany('bit', 'bit32', 'bit.numberlua')
local bxor = bit.bxor
local bnot = bit.bnot
local band = bit.band
local rshift = bit.rshift


local function getIntByte(number, index)
  return band(rshift(number, (8*(index - 1))), 0xff)
end

local function int16ToBytes(number)
  return getIntByte(number, 1),
   getIntByte(number, 2)
end

local function int32ToBytes(number)
  return getIntByte(number, 1),
   getIntByte(number, 2),
    getIntByte(number, 3),
     getIntByte(number, 4)
end

local function serializeInt16(number)
  return string.char(int16ToBytes(number))
end

local function serializeInt32(number)
  return string.char(int32ToBytes(number))
end


local function bytesToChars(t)
  local result = {}
  for k, v in ipairs(t) do
    result[k] = string.char(v)
  end
  return result
end

local function serializeBytes(...)
  local bytes = {...}
  for k, v in ipairs(bytes) do
    bytes[k] = string.char(v)
  end
  return table.concat(bytes)
end


--[[
  Local File Header
]]
local fileHeader = {
  signature = serializeBytes(80, 75, 3, 4), -- Signature
  extractVersion = serializeBytes(10, 0),   -- Version needed to extract
  bitFlag = serializeBytes(0, 0),           -- General purpose bit flag
  method = serializeBytes(0, 0),            -- Compression method
  lastModTime = serializeBytes(0, 0),       -- Last mod file time
  lastModDate = serializeBytes(0, 0),       -- Last mod file date
  crc = serializeBytes(0, 0, 0, 0),         -- CRC-32
  csize = serializeBytes(0, 0, 0, 0),       -- Compressed size (n)
  ucsize = serializeBytes(0, 0, 0, 0),      -- Uncompressed size
  fnlen = serializeBytes(0, 0),             -- Filename length (f)
  extraLen = serializeBytes(0, 0)           -- Extra field length (e)
  -- (f)                                    -- Filename
  -- (e)                                    -- Extra field
  -- (n)                                    -- Compressed data
}

--[[
  PackFileHeader(filename, filenameLen, data, dataLen, crc)
  |  Returns a string containing the FileHeader in binary form.
  |  > filename [str] - The name of the file
  |  > filenameLen [str] - The serialized length of 'filename' ( little-endian )
  |  > data [str] - The file data in binary form
  |  > dataLen [str] - The serialized length of 'data' ( little-endian )
  |  > crc [str] - The serialized CRC-32 checksum
]]
local function packFileHeader(filename, filenameLen, data, dataLen, crc)
  return table.concat({
    fileHeader.signature,
    fileHeader.extractVersion,
    fileHeader.bitFlag,
    fileHeader.method,
    fileHeader.lastModTime,
    fileHeader.lastModDate,
    crc,
    dataLen,
    dataLen,
    filenameLen,
    fileHeader.extraLen,
    filename,
    -- fileHeader.extra,
    data
  })
end

--[[
  NewFileHeader(filename, data, crc)
  |  Returns a string containing the FileHeader in binary form.
  |  > filename [str] - The name of the file
  |  > data [str] - The file data in binary form
  |  > crc [str] - The serialized CRC-32 checksum
]]
local function newFileHeader(filename, data, crc)
  local filenameLen = serializeInt16(filename:len())
  local dataLen = serializeInt32(data:len())
  return packFileHeader(filename, filenameLen, data, dataLen, crc)
end


--[[
  Central Directory
]]
local centralDirectory = {
  signature = serializeBytes(80, 75, 1, 2),   -- Signature
  madeVersion = serializeBytes(10, 0),        -- Version made by
  extractVersion = serializeBytes(10, 0),     -- Version needed to extract
  bitFlag = serializeBytes(0, 0),             -- General purpose bit flag
  method = serializeBytes(0, 0),              -- Compression method
  lastModTime = serializeBytes(0, 0),         -- Last mod file time
  lastModDate = serializeBytes(0, 0),         -- Last mod file date
  crc = serializeBytes(0, 0, 0, 0),           -- CRC-32
  csize = serializeBytes(0, 0, 0, 0),         -- Compressed size
  ucsize = serializeBytes(0, 0, 0, 0),        -- Uncompressed size
  fnlen = serializeBytes(0, 0),               -- Filename length (f)
  extraLen = serializeBytes(0, 0),            -- Extra field length (e)
  commentLen = serializeBytes(0, 0),          -- File comment length (c)
  diskStart = serializeBytes(0, 0),           -- Disk number start
  internAttrib = serializeBytes(0, 0),        -- Internal file attributes
  externAttrib = serializeBytes(0, 0, 0, 0),  -- External file attributes
  headerOffset = serializeBytes(0, 0, 0, 0),  -- Relative offset of local header
  -- (f)                                      -- Filename
  -- (e)                                      -- Extra field
  -- (c)                                      -- File comment
}

--[[
  PackCentralDirectory(filename, filenameLen, dataLen, crc, headerOffset)
  |  Returns a string containing the CentralDirectory in binary form.
  |  > filename [str] - The name of the file
  |  > filenameLen [str] - The serialized length of 'filename' ( little-endian )
  |  > dataLen [str] - The serialized length of the file data ( little-endian )
  |  > crc [str] - The serialized CRC-32 checksum
  |  > headerOffset [str] - The serialized offset of the local header ( little-endian )
]]
local function packCentralDirectory(filename, filenameLen, dataLen, crc, headerOffset)
  return table.concat({
    centralDirectory.signature,
    centralDirectory.madeVersion,
    centralDirectory.extractVersion,
    centralDirectory.bitFlag,
    centralDirectory.method,
    centralDirectory.lastModTime,
    centralDirectory.lastModDate,
    crc,
    dataLen,
    dataLen,
    filenameLen,
    centralDirectory.extraLen,
    centralDirectory.commentLen,
    centralDirectory.diskStart,
    centralDirectory.internAttrib,
    centralDirectory.externAttrib,
    headerOffset,
    filename
    -- centralDirectory.extra,
    -- centralDirectory.fileComment
  })
end

--[[
  NewCentralDirectory(filename, data, crc, headerOffset)
  |  Returns a string containing the CentralDirectory in binary form.
  |  > filename [str] - The name of the file
  |  > data [str] - The file data in binary form
  |  > crc [str] - The serialized CRC-32 checksum
  |  > headerOffset [int] - The offset of the local header
]]
local function newCentralDirectory(filename, data, crc, headerOffset)
  local filenameLen = serializeInt16(filename:len())
  local dataLen = serializeInt32(data:len())
  return packCentralDirectory(filename, filenameLen, dataLen, crc, serializeInt32(headerOffset))
end


--[[
  End of central directory record
]]
local endRecord = {
  signature = serializeBytes(80, 75, 5, 6),   -- Signature
  diskNumber = serializeBytes(0, 0),          -- Number of this disk
  startDiskNumber = serializeBytes(0, 0),     -- Number of the disk with the start of the central directory
  entriesOnDisk = serializeBytes(0, 0),       -- Total number of entries in the central dir on this disk
  totalEntries = serializeBytes(0, 0),        -- Total number of entries in the central dir
  cdSize = serializeBytes(0, 0, 0, 0),        -- Size of the central directory
  cdOffset = serializeBytes(0, 0, 0, 0),      -- Offset of start of central directory with respect to the starting disk number
  commentLen = serializeBytes(0, 0),          -- Zip file comment length (c)
  -- (c)                                      -- Zip file comment
}

--[[
  PackEndRecord(entryCount, cdSize, cdOffset)
  |  Returns a string containing the EndRecord in binary form.
  |  > entryCount [str] - The serialized number of CentralDirectory entries
  |  > cdSize [str] - The serialized size of the CentralDirectory
  |  > cdOffset [str] - The serialized offset of the CentralDirectory
]]
local function packEndRecord(entryCount, cdSize, cdOffset)
  return table.concat({
    endRecord.signature,
    endRecord.diskNumber,
    endRecord.startDiskNumber,
    entryCount,
    entryCount,
    cdSize,
    cdOffset,
    endRecord.commentLen,
    -- endRecord.comment
  })
end

--[[
  NewEndRecord(entryCount, cdSize, cdOffset)
  |  Returns a string containing the EndRecord in binary form.
  |  > entryCount [int] - The number of CentralDirectory entries
  |  > cdSize [int] - The size of the CentralDirectory
  |  > cdOffset [int] - The offset of the CentralDirectory
]]
local function newEndRecord(entryCount, cdSize, cdOffset)
  local sEntryCount = serializeInt16(entryCount)
  local sCdSize = serializeInt32(cdSize)
  local sCdOffset = serializeInt32(cdOffset)
  return packEndRecord(sEntryCount, sCdSize, sCdOffset)
end


local function processFile(filepath, filename, headerOffset)
  local file, openErr = io.open(filepath, "rb")
  if not file then
    return nil, "Failed to open file '"..tostring(filepath).."'> "..openErr
  end
  local filedata = file:read("*all")
  file:close()

  local crc = CRC32.crc32(filedata) % 2^32
  local fh = newFileHeader(filename, filedata, serializeInt32(crc))
  local cd = newCentralDirectory(filename, filedata, serializeInt32(crc), headerOffset)

  return fh, cd
end

function archiveFiles(fileList, archiveFilename)
  if type(fileList) == "string" then
    fileList = {fileList}
  end

  local fileHeaders = {}
  local centralDirs = {}

  local currentHeaderOffset = 0
  local errs = {}
  for k, filepath in ipairs(fileList) do
    local filename = filepath
    if type(filepath) == "table" then
      filename = filepath[2]
      filepath = filepath[1]
    end
    local fh, cd = processFile(filepath, filename, currentHeaderOffset)
    if fh then
      table.insert(fileHeaders, fh)
      table.insert(centralDirs, cd)
      currentHeaderOffset = currentHeaderOffset + fh:len()
    else
      table.insert(errs, cd)
    end
  end

  local fhString = table.concat(fileHeaders)
  local cdString = table.concat(centralDirs)

  local endRecordString = newEndRecord(#fileHeaders, cdString:len(), fhString:len())

  local archiveFile, writeErr = io.open(archiveFilename, "wb")
  if archiveFile then
    archiveFile:write(table.concat({fhString, cdString, endRecordString}))
    archiveFile:close()
  else
    table.insert(errs, "Failed to write to '"..archiveFilename.."'> "..writeErr)
  end

  if #errs > 0 then
    print("Errors\n|  "..table.concat(errs, "\n|  ").."\n/")
  end
end
