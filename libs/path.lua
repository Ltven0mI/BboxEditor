--[[
  MIT License

  Copyright (c) 2017 Wade Rauschenbach

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
]]

local P = {}   -- package
local args = {...}

local type = type
local error = error

local label = args[1]:sub((args[1]:find("%.[^%.]*$") + 1) or 0)
_G[label] = P
local LUAVERSION = _VERSION:gsub("Lua ", "")
LUAVERSION = tonumber(LUAVERSION)
if LUAVERSION >= 5.3 then _ENV = P else setfenv(1, P) end

--[[
  GetDirectory(filepath) : string
  | Returns the root directory of 'filepath'
  | > filepath [str] - The filepath to get the directory of
]]
function getDirectory(filepath)
  -- Type Check filepath
  if type(filepath) ~= "string" then
    error("GetDirectory expected 'string' received '"..type(filepath).."'")
  end

  -- String Match
  local directory = filepath:match("^(.-)[/\\][^/\\]-$")
  return directory or filepath
end

--[[
  GetFilename(filepath) : string
  | Returns the filename of 'filepath'
]]
 