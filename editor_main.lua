love.window.setTitle("BboxEditor - Loading Project...")


local palette = {
  bg={22, 22, 22},
  fg={58, 62, 73},
  fgdark={40, 45, 56},
  fgshadow={0, 0, 0},
  text={220, 220, 220},
  textdark={95, 95, 95}
}

local openProject = nil


-- Base Container --
local container = bbox.ui.container.new(0, 0, 0, 0)
container:setDock(bbox.ui.DOCK_ALL)
function container:onInvalidated()
  local w, h = self:getDockedSize()

  love.graphics.setColor(palette.bg)
  love.graphics.rectangle("fill", 0, 0, w, h)
end

-- Run Button --
local runButton = bbox.ui.button.new(0, 0, 163, 51, "Run", 24)
runButton:setDock(bbox.ui.DOCK_LEFT, bbox.ui.DOCK_TOP)
runButton:setMargin(32, 0, 16, 0)
function runButton:onInvalidated()
  local w, h = self:getDockedSize()

  love.graphics.setColor(palette.fgshadow)
  love.graphics.rectangle("fill", 3, 3, w - 3, h - 3)

  love.graphics.setColor(palette.fg)
  love.graphics.rectangle("fill", 0, 0, w - 3, h - 3)

  self:useFont()

  love.graphics.setColor(palette.text)
  love.graphics.print(self.text, 15, 12)

  self:unuseFont()
end
function runButton:onMousePressed(x, y, btn)
  if openProject then
    os.execute("cd \""..openProject.__path.."/bin/build_01\" && love .\\ && exit")
  end
end
container:addControl(runButton)


function bbox.load(launchData)
  local err = nil
  openProject, err = bbox.project.open(launchData)
  if openProject == nil then
    error(err)
  end

  local builder, err = bbox.project.io.loadBuilder("builders/builder_default/builder.lua")
  if builder == nil then
    error(err)
  end

  local success, err = builder.build(openProject, openProject.__path.."/bin/build_01")
  if not success then
    error(err)
  end

  love.window.setTitle("BboxEditor - "..openProject.name)
end

function bbox.invalidated()
  if container.__invalidated then
    container.__invalidated = false
    container:handle("invalidated")
  end
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.draw(container.__canvas, container.dockedX, container.dockedY)
end

function love.mousepressed(...)
  container:handle("mousepressed", ...)
end

function love.resize(w, h)
  container:handle("parentResized")
  bbox.graphics.invalidate()
end

function love.quit()
  local success, err = openProject:close()
  if not success then
    bbox.log.error("Failed to close project '"..openProject.name.."' "..err)
  end
end