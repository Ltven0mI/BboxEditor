-- BBOXCORE-CALLBACKS --

local P = {} -- package
local args = {...}

local rawget = rawget
local collectgarbage = collectgarbage
local love = love

return function(bboxcore)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  bboxcore[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end

  local getActiveRoom = bboxcore.rooms.getActiveRoom

  function love.directorydropped (dir)
    local room = getActiveRoom()
    if room and rawget(room, "directorydropped") then return room.directorydropped(dir) end
  end

  function love.draw ()
    local room = getActiveRoom()
    if room and rawget(room, "draw") then return room.draw() end
  end

  function love.filedropped (f)
    local room = getActiveRoom()
    if room and rawget(room, "filedropped") then return room.filedropped(f) end
  end

  function love.focus (f)
    local room = getActiveRoom()
    if room and rawget(room, "focus") then return room.focus(f) end
  end

  function love.gamepadaxis (j,a,v)
    local room = getActiveRoom()
    if room and rawget(room, "gamepadaxis") then return room.gamepadaxis(j,a,v) end
  end
  function love.gamepadpressed (j,b)
    local room = getActiveRoom()
    if room and rawget(room, "gamepadpressed") then return room.gamepadpressed(j,b) end
  end
  function love.gamepadreleased (j,b)
    local room = getActiveRoom()
    if room and rawget(room, "gamepadreleased") then return room.gamepadreleased(j,b) end
  end

  function love.joystickadded (j)
    local room = getActiveRoom()
    if room and rawget(room, "joystickadded") then return room.joystickadded(j) end
  end
  function love.joystickaxis (j,a,v)
    local room = getActiveRoom()
    if room and rawget(room, "joystickaxis") then return room.joystickaxis(j,a,v) end
  end
  function love.joystickhat (j,h,v)
    local room = getActiveRoom()
    if room and rawget(room, "joystickhat") then return room.joystickhat(j,h,v) end
  end
  function love.joystickpressed (j,b)
    local room = getActiveRoom()
    if room and rawget(room, "joystickpressed") then return room.joystickpressed(j,b) end
  end
  function love.joystickreleased (j,b)
    local room = getActiveRoom()
    if room and rawget(room, "joystickreleased") then return room.joystickreleased(j,b) end
  end
  function love.joystickremoved (j)
    local room = getActiveRoom()
    if room and rawget(room, "joystickremoved") then return room.joystickremoved(j) end
  end

  function love.keypressed (b,s,r)
    local room = getActiveRoom()
    if room and rawget(room, "keypressed") then return room.keypressed(b,s,r) end
  end
  function love.keyreleased (b,s)
    local room = getActiveRoom()
    if room and rawget(room, "keyreleased") then return room.keyreleased(b,s) end
  end

  function love.load (arg)
    local room = getActiveRoom()
    if room and rawget(room, "load") then return room.load(arg) end
  end

  function love.lowmemory ()
    local room = getActiveRoom()
    if room and rawget(room, "lowmemory") then room.lowmemory() end
    collectgarbage()
    collectgarbage()
  end

  function love.mousefocus (f)
    local room = getActiveRoom()
    if room and rawget(room, "mousefocus") then return room.mousefocus(f) end
  end
  function love.mousemoved (x,y,dx,dy,t)
    local room = getActiveRoom()
    if room and rawget(room, "mousemoved") then return room.mousemoved(x,y,dx,dy,t) end
  end
  function love.mousepressed (x,y,b,t)
    local room = getActiveRoom()
    if room and rawget(room, "mousepressed") then return room.mousepressed(x,y,b,t) end
  end
  function love.mousereleased (x,y,b,t)
    local room = getActiveRoom()
    if room and rawget(room, "mousereleased") then return room.mousereleased(x,y,b,t) end
  end

  function love.quit ()
    local room = getActiveRoom()
    if room and rawget(room, "quit") then return room.quit() end
  end

  function love.resize (w, h)
    local room = getActiveRoom()
    if room and rawget(room, "resize") then return room.resize(w, h) end
  end

  function love.textedited (t,s,l)
    local room = getActiveRoom()
    if room and rawget(room, "textedited") then return room.textedited(t,s,l) end
  end
  function love.textinput (t)
    local room = getActiveRoom()
    if room and rawget(room, "textinput") then return room.textinput(t) end
  end

  function love.threaderror (t, err)
    local room = getActiveRoom()
    if room and rawget(room, "threaderror") then return room.threaderror(t, err) end
  end

  function love.touchmoved (id,x,y,dx,dy,p)
    local room = getActiveRoom()
    if room and rawget(room, "touchmoved") then return room.touchmoved(id,x,y,dx,dy,p) end
  end
  function love.touchpressed (id,x,y,dx,dy,p)
    local room = getActiveRoom()
    if room and rawget(room, "touchpressed") then return room.touchpressed(id,x,y,dx,dy,p) end
  end
  function love.touchreleased (id,x,y,dx,dy,p)
    local room = getActiveRoom()
    if room and rawget(room, "touchreleased") then return room.touchreleased(id,x,y,dx,dy,p) end
  end

  function love.update (dt)
    local room = getActiveRoom()
    if room and rawget(room, "update") then return room.update(dt) end
  end

  function love.visible (v)
    local room = getActiveRoom()
    if room and rawget(room, "visible") then return room.visible(v) end
  end

  function love.wheelmoved (x,y)
    local room = getActiveRoom()
    if room and rawget(room, "wheelmoved") then return room.wheelmoved(x,y) end
  end

end