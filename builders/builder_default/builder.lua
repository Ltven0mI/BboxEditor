-- BUILDER-DEFAULT --

local P = {} -- Builder

local BboxLib = BboxLib
local pairs = pairs
local table = table

return function(bbox, dir)

  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end

  -- Load Template Files
  local tmp_main, err = bbox.io.importFile(dir.."/template_main.lua")
  if tmp_main == nil then
    return nil, "Failed to load template_main: "..err
  end

  local tmp_init, err = bbox.io.importFile(dir.."/template_init.lua")
  if tmp_init == nil then
    return nil, "Failed to load template_init: "..err
  end

  local tmp_callbacks, err = bbox.io.importFile(dir.."/template_callbacks.lua")
  if tmp_callbacks == nil then
    return nil, "Failed to load template_callbacks: "..err
  end

  local tmp_rooms, err = bbox.io.importFile(dir.."/template_rooms.lua")
  if tmp_rooms == nil then
    return nil, "Failed to load template_rooms: "..err
  end



  --[[
    createDirectories(buildPath) : bool | nil, string
    | Returns true on success otherwise nil and an error message.
    | > buildPath [str] - The path to create directories in
  ]]
  local function createDirectories(buildPath)
    local success, err = nil, nil

    local path = buildPath
    success, err = BboxLib.createDirectory(path)
    if not success then
      return nil, "Failed to create directory '"..path.."' "..err
    end

    local path = buildPath.."/bboxcore"
    success, err = BboxLib.createDirectory(path)
    if not success then
      return nil, "Failed to create directory '"..path.."' "..err
    end

    local path = buildPath.."/rooms"
    success, err = BboxLib.createDirectory(path)
    if not success then
      return nil, "Failed to create directory '"..path.."' "..err
    end

    return true
  end

  --[[
    build(proj, buildPath) : bool | nil, string
    | Returns true on success otherwise nil and an error message.
    | > proj [tbl] - A table containing the project to build
    | > buildPath [str] - The path to save the built project to
  ]]
  function build(proj, buildPath)
    -- Type Check proj
    local success, err = bbox.types.checktype(proj, "table")
    if not success then return nil, err end
    -- Type Check buildPath
    local success, err = bbox.types.checktype(buildPath, "string")
    if not success then return nil, err end

    local success, err = createDirectories(buildPath)
    if not success then
      return nil, err
    end

    bbox.io.exportFile(buildPath.."/main.lua", tmp_main)
    bbox.io.exportFile(buildPath.."/bboxcore/init.lua", tmp_init)
    bbox.io.exportFile(buildPath.."/bboxcore/callbacks.lua", tmp_callbacks)

    -- Load Templates and store in proj.__loadedTemplates
    local loadedTemplates = {}
    for templateName, templatePath in pairs(proj.__templatePaths) do
      local loadedTemplate, err = bbox.project.io.loadTemplate(templatePath, proj.__path)
      if loadedTemplate == nil then
        return nil, "Failed to load template '"..templateName.."' "..err
      end
      loadedTemplates[templateName] = loadedTemplate
    end
    proj.__loadedTemplates = loadedTemplates


    -- Get rooms.lua code to write
    local roomsFileCode = tmp_rooms

    -- Get Room Entries --
    local roomPathEntries = {}
    local roomLoadEntries = {}

    local buildRoom = bbox.project.room.buildRoom
    for name, roomInfoTable in pairs(proj.roomInfoTables) do
      local roomPathEntry = name.."=\""..roomInfoTable.__path.."\""
      table.insert(roomPathEntries, roomPathEntry)

      local roomLoadEntry = "__errLoadRoom(\""..name.."\")"
      table.insert(roomLoadEntries, roomLoadEntry)

      -- Build and write each room's code
      local roomCode, err = buildRoom(roomInfoTable, proj)
      if roomCode == nil then
        return nil, "Failed to build room '"..roomInfoTable.name.."' "..err
      end
      local success, err = bbox.io.exportFile(buildPath.."/"..roomInfoTable.__path, roomCode)
      if not success then
        return nil, "Failed to write room '"..roomInfoTable.name.."' to file: "..err
      end
    end

    -- Concat entries into code snippets
    local roomPathStr = table.concat(roomPathEntries, ",\n")
    local roomLoadStr = table.concat(roomLoadEntries, "\n")
    local roomStartStr = "__errSetActiveRoom(\""..proj.__startRoom.."\")"

    -- Replace keywords with corresponding code snippets
    roomsFileCode = roomsFileCode:gsub("##ROOMPATHS##", roomPathStr)
    roomsFileCode = roomsFileCode:gsub("##LOADROOMS##", roomLoadStr)
    roomsFileCode = roomsFileCode:gsub("##STARTROOM##", roomStartStr)

    -- Write rooms.lua code to file
    local success, err = bbox.io.exportFile(buildPath.."/bboxcore/rooms.lua", roomsFileCode)
    if not success then
      return nil, "Failed to write rooms.lua code to file: "..err
    end

    -- Clean up temporary tables
    proj.__loadedTemplates = nil

    return true
  end


  -- Return Builder
  return P

end