-- BBOXCORE-INIT --

local P = {} -- package
local args = {...}

local require = require

local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
_G[label] = P
_VERSIONNUM = _VERSION:gsub("Lua ", "")
_VERSIONNUM = tonumber(_VERSIONNUM)
if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end

require(args[1]..".rooms")(P)     -- Rooms
require(args[1]..".callbacks")(P) -- Callbacks