-- BBOXCORE-ROOMS --

local P = {} -- package
local args = {...}

local loadfile = loadfile
local error = error
local type = type

return function(bboxcore)

  local label = args[1]:sub(((args[1]:find("%.[^%.]*$") or -1) + 1) or 0)
  bboxcore[label] = P
  if _VERSIONNUM >= 5.3 then _ENV = P else setfenv(1, P) end

  --[[
    SetActiveRoom(roomname)
    | Returns true on success otherwise nil and an error message.
    | > roomname [str] - The name of the room to make active
  ]]
  function setActiveRoom(roomname)
    -- Type Check roomname
    if type(roomname) ~= "string" then
      return nil, "Type mismatch: Expected 'string' received '"..type(roomname).."'"
    end

    -- Check For Loaded Room
    if _loadedRooms[roomname] then
      if _activeRoom and type(_activeRoom.onDeactivated) == "function" then
        _activeRoom.onDeactivated()
      end
      _activeRoom = _loadedRooms[roomname]
      if type(_activeRoom.onActivated) == "function" then
        _activeRoom.onActivated()
      end
      return true
    end

    -- Check For Room Path
    if _roomPaths[roomname] == nil then
      return nil, "No such room '"..roomname.."'"
    end

    -- Load Room
    local loadedRoom, msg = loadRoom(roomname)
    if loadedRoom == nil then
      return nil, msg
    end

    -- Set Active Room
    if _activeRoom and type(_activeRoom.onDeactivated) == "function" then
      _activeRoom.onDeactivated()
    end
    _activeRoom = loadedRoom
    if type(_activeRoom.onActivated) == "function" then
      _activeRoom.onActivated()
    end
    return true
  end

  --[[
    GetActiveRoom()
    | Returns the active room or nil if no active room.
  ]]
  function getActiveRoom()
    return _activeRoom
  end

  local function __errSetActiveRoom(roomname)
    local success, msg = setActiveRoom(roomname)
    if not success then
      error(msg, 2)
    end
  end

  --[[
    LoadRoom(roomname)
    | Returns loaded room on success otherwise nil and an error message.
    | > roomname [str] - The name of the room to load
  ]]
  function loadRoom(roomname)
    -- Type Check roomname
    if type(roomname) ~= "string" then
      return nil, "Type mismatch: Expected 'string' received '"..type(roomname).."'"
    end

    local roomPath = _roomPaths[roomname]
    local roomFunc, msg = loadfile(roomPath)
    if not roomFunc then
      return nil, "Failed to load room '"..roomPath.."': "..msg
    end
    local loadedRoom = roomFunc()
    _loadedRooms[roomname] = loadedRoom
    return loadedRoom
  end

  local function __errLoadRoom(roomname)
    local room, msg = loadRoom(roomname)
    if not room then
      error(msg, 2)
    end
  end

  _roomPaths = {
    ##ROOMPATHS##
  }

  _loadedRooms = {}

  function __initRooms()
    ##LOADROOMS##

    ##STARTROOM##
  end

end